from scipy.integrate import quad
from numpy import log, sin, cos, tan

def integrand(x):
	return sin(cos(tan(x)))
    
print(quad(integrand, 0, 5))